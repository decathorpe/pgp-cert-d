use assert_cmd::Command;
use assert_fs::prelude::*;
use openpgp::parse::Parse;
use openpgp::serialize::SerializeInto;
use openpgp::Cert;
use sequoia_openpgp as openpgp;

#[test]
fn test_insert() -> anyhow::Result<()> {
    let cert =
        Cert::from_bytes(include_bytes!("../../testdata/testy-new.pgp"))?;

    let base = assert_fs::TempDir::new().unwrap();

    // Insert the cert.
    // Use --store to set the store's path
    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))?
        .arg("insert")
        .args(&["--store", base.path().to_str().unwrap()])
        .write_stdin(cert.to_vec()?)
        .assert();
    assert.success();

    let read_cert = Cert::from_bytes(&std::fs::read(
        base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1"),
    )?)?;
    assert_eq!(cert, read_cert);

    // Insert the cert again.
    // Use the environment variable to set the store's path
    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))?
        .arg("insert")
        .env("PGP_CERT_D", base.path().to_str().unwrap())
        .write_stdin(cert.to_vec()?)
        .assert();
    assert.success();

    let read_cert = Cert::from_bytes(&std::fs::read(
        base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1"),
    )?)?;
    assert_eq!(cert, read_cert);

    Ok(())
}
