use assert_cmd::Command;
use assert_fs::prelude::*;

use openpgp::parse::Parse;
use openpgp::serialize::SerializeInto;
use openpgp::Cert;
use sequoia_openpgp as openpgp;

#[test]
fn test_export() -> anyhow::Result<()> {
    // Setup new store with three certs
    let base = assert_fs::TempDir::new().unwrap();

    let alice = Cert::from_bytes(include_bytes!("../../testdata/alice.asc"))?
        .to_vec()?;
    base.child("eb/85bb5fa33a75e15e944e63f231550c4f47e38e")
        .write_binary(&alice)
        .unwrap();
    let bob =
        Cert::from_bytes(include_bytes!("../../testdata/bob.asc"))?.to_vec()?;
    base.child("d1/a66e1a23b182c9980f788cfbfcc82a015e7330")
        .write_binary(&bob)
        .unwrap();
    let testy =
        Cert::from_bytes(include_bytes!("../../testdata/testy-new.pgp"))?
            .to_vec()?;
    base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1")
        .write_binary(&testy)
        .unwrap();

    // Get the certs
    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .unwrap()
        .arg("export")
        .args(&["--store", base.path().to_str().unwrap()])
        .assert();
    let assert = assert.success();

    // Check the output.
    // The certs may be in any order.
    let output = &assert.get_output().stdout.as_slice();
    if output.starts_with(&alice) {
        if output[alice.len()..].starts_with(&bob) {
            //ABT
            assert!(output[alice.len() + bob.len()..].starts_with(&testy));
            assert!(output.ends_with(&testy))
        } else {
            //ATB
            assert!(output[alice.len()..].starts_with(&testy));
            assert!(output[alice.len() + testy.len()..].starts_with(&bob));
            assert!(output.ends_with(&bob))
        }
    } else if output.starts_with(&bob) {
        if output[bob.len()..].starts_with(&alice) {
            //BAT
            assert!(output[bob.len() + alice.len()..].starts_with(&testy));
            assert!(output.ends_with(&testy))
        } else {
            //BTA
            assert!(output[bob.len()..].starts_with(&testy));
            assert!(output[bob.len() + testy.len()..].starts_with(&alice));
            assert!(output.ends_with(&alice))
        }
    } else {
        assert!(output.starts_with(&testy));
        if output[testy.len()..].starts_with(&alice) {
            //TAB
            assert!(output[testy.len() + alice.len()..].starts_with(&bob));
            assert!(output.ends_with(&bob))
        } else {
            //TBA
            assert!(output[testy.len()..].starts_with(&bob));
            assert!(output[testy.len() + bob.len()..].starts_with(&alice));
            assert!(output.ends_with(&alice))
        }
    };

    Ok(())
}

#[test]
fn test_export_empty() -> anyhow::Result<()> {
    let base = assert_fs::TempDir::new().unwrap();

    // There are no certs.
    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .unwrap()
        .arg("export")
        .args(&["--store", base.path().to_str().unwrap()])
        .assert();
    let assert = assert.success();

    assert.get_output().stdout.is_empty();
    Ok(())
}
