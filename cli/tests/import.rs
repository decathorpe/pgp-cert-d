use assert_cmd::Command;
use assert_fs::prelude::*;

use openpgp::parse::Parse;
use openpgp::Cert;
use sequoia_openpgp as openpgp;

#[test]
fn test_import() -> anyhow::Result<()> {
    // 1. Load bytes of several binary certs, just append them
    // 2. Import
    // 3. Assert each cert is in the store
    // 4. Assert not more is in the store
    let testy = include_bytes!("../../testdata/testy-new.pgp");
    let alice = include_bytes!("../../testdata/alice.pgp");
    let bob = include_bytes!("../../testdata/bob.pgp");

    let certring = [testy.as_ref(), alice.as_ref(), bob.as_ref()].concat();

    let base = assert_fs::TempDir::new().unwrap();

    // Import the keyring.
    // Use --store to set the store's path
    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))?
        .arg("import")
        .args(&["--store", base.path().to_str().unwrap()])
        .write_stdin(certring.clone())
        .assert();
    assert.success();

    let read_cert = Cert::from_bytes(&std::fs::read(
        base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1"),
    )?)?;
    assert_eq!(Cert::from_bytes(testy)?, read_cert);

    let read_cert = Cert::from_bytes(&std::fs::read(
        base.child("eb/85bb5fa33a75e15e944e63f231550c4f47e38e"),
    )?)?;
    assert_eq!(Cert::from_bytes(alice)?, read_cert);

    let read_cert = Cert::from_bytes(&std::fs::read(
        base.child("d1/a66e1a23b182c9980f788cfbfcc82a015e7330"),
    )?)?;
    assert_eq!(Cert::from_bytes(bob)?, read_cert);

    // Check that nothing else is in the store
    let certd = openpgp_cert_d::CertD::with_base_dir(&base)?;
    assert_eq!(certd.fingerprints().flatten().count(), 3);

    // Import the keyring again, no files should be added or changed.
    // Use the environment variable to set the store's path
    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))?
        .arg("import")
        .env("PGP_CERT_D", base.path().to_str().unwrap())
        .write_stdin(certring)
        .assert();
    assert.success();

    let read_cert = Cert::from_bytes(&std::fs::read(
        base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1"),
    )?)?;
    assert_eq!(Cert::from_bytes(testy)?, read_cert);

    let read_cert = Cert::from_bytes(&std::fs::read(
        base.child("eb/85bb5fa33a75e15e944e63f231550c4f47e38e"),
    )?)?;
    assert_eq!(Cert::from_bytes(alice)?, read_cert);

    let read_cert = Cert::from_bytes(&std::fs::read(
        base.child("d1/a66e1a23b182c9980f788cfbfcc82a015e7330"),
    )?)?;
    assert_eq!(Cert::from_bytes(bob)?, read_cert);

    // Check that nothing else is in the store
    let certd = openpgp_cert_d::CertD::with_base_dir(&base)?;
    assert_eq!(certd.fingerprints().flatten().count(), 3);

    Ok(())
}
