use assert_cmd::Command;
use assert_fs::prelude::*;
use openpgp::parse::Parse;
use openpgp::Cert;
use sequoia_openpgp as openpgp;

#[test]
fn setup_create_simple() -> anyhow::Result<()> {
    let base = assert_fs::TempDir::new().unwrap();
    let userid = "trust-root";

    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .unwrap()
        .arg("setup")
        .args(&["--store", base.path().to_str().unwrap()])
        .assert();
    assert.success();

    let trust_root =
        Cert::from_bytes(&std::fs::read(base.child("trust-root"))?)?;

    // Check the userid, and that it is exactly one
    let p = &openpgp::policy::StandardPolicy::new();
    let valid_trust_root = trust_root.with_policy(p, None)?;

    // Check the primary userid
    assert_eq!(
        userid,
        valid_trust_root.primary_userid().unwrap().name()?.unwrap()
    );
    // And that there is unecrypted secret key material.
    assert!(valid_trust_root
        .primary_key()
        .key()
        .has_unencrypted_secret());

    assert_created_trust_root_props(valid_trust_root)
}

fn assert_created_trust_root_props(
    vc: openpgp::cert::ValidCert,
) -> anyhow::Result<()> {
    assert_eq!(1, vc.userids().count());

    // The primary key is certification capable
    assert!(vc.primary_key().for_certification());
    // and it has secret key material.
    assert!(vc.primary_key().key().has_secret());

    // There are no subkeys
    assert_eq!(1, vc.keys().count());

    // The Direct Key Signature must be non-exportable
    let dks = vc.direct_key_signature()?;
    assert!(matches!(dks, openpgp::packet::Signature::V4(_)));
    match dks {
        openpgp::packet::Signature::V4(sig) => {
            assert!(sig.exportable().is_err())
        }
        &_ => unreachable!(),
    }

    // Assert that the primary userid's binding signature is non-exportable
    let p = &openpgp::policy::StandardPolicy::new();
    let pu = vc.primary_userid()?;
    let bs = pu.binding_signature(p, None)?;
    assert!(matches!(bs, openpgp::packet::Signature::V4(_)));
    match bs {
        openpgp::packet::Signature::V4(sig) => {
            assert!(sig.exportable().is_err())
        }
        &_ => unreachable!(),
    }
    Ok(())
}

#[test]
fn setup_import() -> anyhow::Result<()> {
    let base = assert_fs::TempDir::new().unwrap();
    let trust_root_bytes = include_bytes!("../../testdata/trust-root");

    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .unwrap()
        .arg("setup")
        .args(&["--store", base.path().to_str().unwrap()])
        .arg("--import-from-stdin")
        .write_stdin(trust_root_bytes.to_vec())
        .assert();
    assert.success();

    let read_trust_root =
        Cert::from_bytes(&std::fs::read(base.child("trust-root"))?)?;

    assert_eq!(Cert::from_bytes(trust_root_bytes)?, read_trust_root);
    Ok(())
}
