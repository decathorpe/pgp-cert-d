use assert_cmd::Command;
use assert_fs::prelude::*;
use openpgp::parse::Parse;
use openpgp::Cert;
use sequoia_openpgp as openpgp;

#[test]
fn test_get_nonexistent() {
    let base = assert_fs::TempDir::new().unwrap();
    // Try to get a cert
    // Use --store to set the store's path
    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .unwrap()
        .arg("get")
        .arg("39d100ab67d5bd8c04010205fb3751f1587daef1")
        .args(&["--store", base.path().to_str().unwrap()])
        .assert();
    assert.failure();
    // XXX: test correct exit code
}

#[test]
fn test_get() {
    // Setup new store with one cert
    let data = include_bytes!("../../testdata/testy-new.pgp");
    let base = assert_fs::TempDir::new().unwrap();
    base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1")
        .write_binary(data)
        .unwrap();

    // Get the cert.
    // Use --store to set the store's path
    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .unwrap()
        .arg("get")
        .arg("39d100ab67d5bd8c04010205fb3751f1587daef1")
        .args(&["--store", base.path().to_str().unwrap()])
        .assert();
    let assert = assert.success();

    let output_cert = Cert::from_bytes(&assert.get_output().stdout).unwrap();
    assert_eq!(output_cert, Cert::from_bytes(data).unwrap());

    // Get the cert again.
    // Use the environment variable to set the store's path
    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .unwrap()
        .arg("get")
        .arg("39d100ab67d5bd8c04010205fb3751f1587daef1")
        .env("PGP_CERT_D", base.path().to_str().unwrap())
        .assert();
    let assert = assert.success();

    let output_cert = Cert::from_bytes(&assert.get_output().stdout).unwrap();
    assert_eq!(output_cert, Cert::from_bytes(data).unwrap());
}

#[test]
fn test_get_uppercase() {
    // Setup new store with one cert
    let data = include_bytes!("../../testdata/testy-new.pgp");
    let base = assert_fs::TempDir::new().unwrap();
    base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1")
        .write_binary(data)
        .unwrap();

    // Specify the cert with an uppercase fingerprint.
    let assert = Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .unwrap()
        .arg("get")
        .arg("39D100AB67D5BD8C04010205FB3751F1587DAEF1")
        .args(&["--store", base.path().to_str().unwrap()])
        .assert();
    let assert = assert.success();

    let output_cert = Cert::from_bytes(&assert.get_output().stdout).unwrap();
    assert_eq!(output_cert, Cert::from_bytes(data).unwrap());
}
