use clap::Parser;

use std::path::PathBuf;

mod cli;
use sequoia_public_store::{Result, Store};

use openpgp::crypto::Password;
use openpgp::policy::StandardPolicy;
use openpgp::Fingerprint;
use sequoia_openpgp as openpgp;

pub const POLICY: StandardPolicy = StandardPolicy::new();

fn main() -> Result<()> {
    let opt = cli::PgpCertD::parse();

    match opt {
        cli::PgpCertD::Get { store, fingerprint } => get(store, fingerprint),
        cli::PgpCertD::Insert { store } => insert(store),
        cli::PgpCertD::Import { store } => import(store),
        cli::PgpCertD::Export { store } => export(store),
        cli::PgpCertD::Setup {
            store,
            import_from_stdin,
            ..
        } if import_from_stdin => setup_import_stdin(store),
        cli::PgpCertD::Setup {
            store,
            with_password,
            ..
        } => setup_create(store, with_password),
    }
}

fn get(store: Option<PathBuf>, fingerprint: String) -> Result<()> {
    let certd = Store::new(store)?;

    let fingerprint = Fingerprint::from_hex(&fingerprint)?;

    certd.get_export(&fingerprint, &mut std::io::stdout())
}

fn insert(store: Option<PathBuf>) -> Result<()> {
    let certd = Store::new(store)?;

    certd.insert_impl(std::io::stdin())
}

fn import(store: Option<PathBuf>) -> Result<()> {
    let certd = Store::new(store)?;

    certd.import_impl(std::io::stdin())
}

fn export(store: Option<PathBuf>) -> Result<()> {
    let certd = Store::new(store)?;

    certd.export_impl(&mut std::io::stdout())
}

// Setup a new certificate directory and create a trust-root.
//
// The created trust-root
// - has a userid "trust-root", for compatibility
// - optionally a password
// - certification capable primary key
// - no subkeys
// - the direct key signature and the primary userid's binding signature are
//   marked non-exportable.
//
// See 3.5.1 for the trust-root's specification.
fn setup_create(store: Option<PathBuf>, with_password: bool) -> Result<()> {
    let certd = Store::new(store)?;

    let password = if with_password {
        Some(read_new_password()?)
    } else {
        None
    };

    certd.setup_create_impl(password)
}

// Read a new password from stdin and double check.
fn read_new_password() -> anyhow::Result<Password> {
    let p0 = {
        rpassword::prompt_password("Enter password to protect the key: ")?
            .into()
    };
    let p1: Password = {
        rpassword::prompt_password("Repeat the password once more: ")?.into()
    };

    if p0 == p1 {
        Ok(p0)
    } else {
        Err(anyhow::anyhow!("Passwords do not match."))
    }
}

// Import the trust-root from stdin.
fn setup_import_stdin(store: Option<PathBuf>) -> Result<()> {
    let certd = Store::new(store)?;

    certd.setup_import_stdin_impl(std::io::stdin())
}
