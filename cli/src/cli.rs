use clap::Parser;
use std::path::PathBuf;

#[derive(Debug, Parser)]
#[clap(about = "A CLI for the the \
                     Shared PGP Certificate Directory")]
pub enum PgpCertD {
    /// Looks up a certificate by its fingerprint
    ///
    /// If found, writes the cert to stdout.
    Get {
        /// The path of the store
        #[clap(short, long)]
        store: Option<PathBuf>,
        /// The fingerprint
        fingerprint: String,
    },
    /// Inserts or updates a certificate
    ///
    /// Reads the cert from stdin
    Insert {
        /// The path of the store
        #[clap(short, long)]
        store: Option<PathBuf>,
    },
    /// Import certificates into the store from stdin.
    Import {
        /// The path of the store
        #[clap(short, long)]
        store: Option<PathBuf>,
    },
    /// Exports all certificates in the store to stdout.
    Export {
        /// The path of the store
        #[clap(short, long)]
        store: Option<PathBuf>,
    },
    /// Setup a new store
    ///
    /// Import the trust-root or create a new one.
    Setup {
        /// The path of the store
        #[clap(short, long, global(true))]
        store: Option<PathBuf>,
        /// Ask for a password
        #[clap(long = "with-password", conflicts_with("import-from-stdin"))]
        with_password: bool,
        /// Import from stdin
        #[clap(long = "import-from-stdin", conflicts_with("with-password"))]
        import_from_stdin: bool,
    },
}
