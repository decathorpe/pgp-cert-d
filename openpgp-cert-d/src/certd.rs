use std::{
    convert::TryInto,
    env, fs,
    io::{self, Read, Write},
    path::{Path, PathBuf},
};

use fd_lock::RwLock;
use tempfile::NamedTempFile;
use walkdir::WalkDir;

use crate::SPECIAL_NAMES;
use crate::{pgp, InternalError};
use crate::{Data, Error, Result, Tag};

const PATH_PREFIX_LEN: usize = 2;

/// A certificate store.
///
/// This is a handle to an on-disk certificate store that can be used
/// to lookup and insert certificates.
#[derive(Debug)]
pub struct CertD {
    base: PathBuf,
}

impl CertD {
    /// Opens the default certificate store.
    ///
    /// If not explicitly requested otherwise, an application SHOULD
    /// use the [default store].  To use a store with a different
    /// location, use [`CertD::with_base_dir`].
    ///
    /// [default store]: https://www.ietf.org/archive/id/draft-nwjw-openpgp-cert-d-00.html#name-default-stores-location
    pub fn new() -> Result<CertD> {
        CertD::with_base_dir(
            env::var_os("PGP_CERT_D")
                .map(Into::into)
                .unwrap_or_else(CertD::default_location),
        )
    }

    fn default_location() -> PathBuf {
        // XXX: Other platforms?
        dirs::data_dir()
            .expect("Unsupported platform")
            .join("pgp.cert.d")
    }

    /// Opens a store with an explicit location.
    ///
    /// Note: If not explicitly requested otherwise, an application
    /// SHOULD use the [default store] using [`CertD::new`].
    ///
    /// [default store]: https://www.ietf.org/archive/id/draft-nwjw-openpgp-cert-d-00.html#name-default-stores-location
    pub fn with_base_dir<P: AsRef<Path>>(base: P) -> Result<CertD> {
        Ok(CertD {
            base: base.as_ref().into(),
        })
    }

    /// Get the this Certd's base path.
    pub fn get_base_dir(&self) -> &Path {
        &self.base
    }

    /// Turns a fingerprint into a path in the store, according to cert-d
    /// specification.
    fn get_path_by_fp(&self, fingerprint: &str) -> Result<PathBuf> {
        if fingerprint.len() != 40 {
            return Err(Error::BadName);
        }
        if fingerprint.chars().any(|c| !c.is_ascii_hexdigit()) {
            return Err(Error::BadName);
        }
        let fingerprint = fingerprint.to_ascii_lowercase();
        Ok(self.base.join(&fingerprint[..2]).join(&fingerprint[2..]))
    }

    /// Turns a path in the store into a fingerprint, if it conforms to the cert-d
    /// specification.
    fn get_fp_by_path(
        &self,
        path: &Path,
    ) -> std::result::Result<String, InternalError> {
        let path = if path.is_absolute() {
            path.strip_prefix(&self.base)
                .map_err(|_| InternalError::PathNotInStore)?
        } else {
            path
        };
        if !self.base.join(path).is_file() {
            return Err(InternalError::BadFingerprintPath);
        }
        if path.components().count() != 2 {
            return Err(InternalError::BadFingerprintPath);
        }
        let components =
            path.components().map(|c| c.as_os_str()).collect::<Vec<_>>();
        if components.iter().any(|c| !c.is_ascii()) {
            return Err(InternalError::BadFingerprintPath);
        }
        let head = components[0].to_string_lossy();
        if head.len() != PATH_PREFIX_LEN {
            return Err(InternalError::BadFingerprintPath);
        }
        let tail = components[1].to_string_lossy();
        if tail.len() != pgp::FP_LEN_CHARS_V4 - PATH_PREFIX_LEN
            && tail.len() != pgp::FP_LEN_CHARS_V5 - PATH_PREFIX_LEN
        {
            return Err(InternalError::BadFingerprintPath);
        }
        Ok(head.to_string() + &tail)
    }

    /// Turns a special name into a path in the store.
    fn get_path_by_special(&self, special: &str) -> Result<PathBuf> {
        let special = special.to_lowercase();
        if SPECIAL_NAMES.binary_search(&special.as_ref()).is_ok() {
            Ok(self.base.join(special))
        } else {
            Err(Error::BadName)
        }
    }

    /// Looks up a certificate in the store by an name, i.e. a fingerprint
    /// or a special name.
    ///
    /// If the certificate exists, this function returns `Ok(Some((tag,
    /// cert)))`.  See [`Tag`] for how this can be used to cache lookups.
    ///
    /// If the certificate does not exist, this function returns `Ok(None)`.
    ///
    /// If an I/O error occurs, or the name was invalid, this function returns
    /// an [`Error`].
    pub fn get(&self, name: &str) -> Result<Option<(Tag, Data)>> {
        let path = self.get_path(name)?;
        match fs::File::open(path) {
            Ok(mut f) => {
                let tag = f.metadata()?.try_into()?;
                let mut buf = Vec::new();
                f.read_to_end(&mut buf)?;
                Ok(Some((tag, buf.into())))
            }
            Err(e) if e.kind() == io::ErrorKind::NotFound => Ok(None),
            Err(e) => Err(e.into()),
        }
    }

    /// Looks up a certificate in the store by an name, i.e. a fingerprint
    /// or a special name, with the [`Tag`] from the previous lookup.
    ///
    /// If the certificate has changed, this function returns `Ok(Some((cert,
    /// tag)))`.  The tag can be used in subsequent calls to this function.
    ///
    /// If the certificate has not changed or does not exist, this function
    /// returns `Ok(None)`.
    ///
    /// If an I/O error occurs, or the name was invalid, this function returns
    /// an [`Error`].
    pub fn get_if_changed(
        &self,
        since: Tag,
        name: &str,
    ) -> Result<Option<(Tag, Data)>> {
        let path = self.get_path(name)?;
        match fs::File::open(path) {
            Ok(mut f) => {
                let tag = f.metadata()?.try_into()?;
                if since == tag {
                    Ok(None) // Not modified.
                } else {
                    let mut buf = Vec::new();
                    f.read_to_end(&mut buf)?;
                    Ok(Some((tag, buf.into())))
                }
            }
            Err(e) if e.kind() == io::ErrorKind::NotFound => Ok(None),
            Err(e) => Err(e.into()),
        }
    }

    /// Get the path to a certificate.
    pub fn get_path(&self, name: &str) -> Result<PathBuf> {
        // Try to convert the name to a path, first as a fingerprint and
        // if that fails as a special name.
        // If the errors get more insightful than just Error::BadName, prefer
        // returning the one from fp_to_path.
        self.get_path_by_fp(name)
            .or_else(|_| self.get_path_by_special(name))
    }

    /// Inserts or updates a cert.
    ///
    /// Requires the fingerprint and a callback function.  The callback is
    /// invoked with an `Option<Data>` of the existing cert data (if any),
    /// and is expected to merge the two copies of the certificate together.
    /// The returned `Data` is written to the store.
    /// (Note: The function may decide to omit (parts of) the existing data,
    /// but this should be done with great care as not to lose any vital
    /// information.)
    ///
    /// Acquires lock to the store, blocking the current thread until it's able
    /// to do so.
    ///
    /// The insertion method returns the merged certificate data and the tag.
    /// See [`Tag`] for how this can be used to cache lookups.
    pub fn insert<M>(&self, data: Data, merge: M) -> Result<(Tag, Data)>
    where
        M: FnOnce(Data, Option<Data>) -> Result<Data>,
    {
        let blocking = true;
        let name = pgp::fingerprint(data.as_ref())?;
        self.insert_impl(&name, data, merge, blocking)
    }

    /// Inserts or updates a cert, non-blocking variant.
    ///
    /// Requires the fingerprint and a callback function.  The callback is
    /// invoked with an `Option<Data>` of the existing cert data (if any),
    /// and is expected to merge the two copies of the certificate together.
    /// The returned `Data` is written to the store.
    /// (Note: The function may decide to omit (parts of) the existing data,
    /// but this should be done with great care as not to lose any vital
    /// information.)
    ///
    /// Attempts to lock the store. Does not block, instead returns an
    /// [`Error::IoError`] with an [`std::io::ErrorKind::WouldBlock`]
    /// if the lock cannot be acquired.
    ///
    /// The insertion method returns the merged certificate data and the tag.
    /// See [`Tag`] for how this can be used to cache lookups.
    pub fn try_insert<M>(&self, data: Data, merge: M) -> Result<(Tag, Data)>
    where
        M: FnOnce(Data, Option<Data>) -> Result<Data>,
    {
        let blocking = false;
        let name = pgp::fingerprint(data.as_ref())?;
        self.insert_impl(&name, data, merge, blocking)
    }

    /// Inserts or updates the cert or key stored under a special name.
    ///
    /// Requires the special name, the cert or key in binary format and a
    /// callback function.  The callback is invoked with an `Option<Data>` of
    /// the existing data (if any), and is expected to merge the two copies
    /// together.  The returned `Data` is written to the
    /// store under the special name.  (Note: The function may decide to omit
    /// (parts of) the existing data, but this should be done with great care as
    /// not to lose any vital information.)
    ///
    /// Acquires lock to the store, blocking the current thread until it's able
    /// to do so.
    ///
    /// The insertion method returns the merged data and the tag.
    /// See [`Tag`] for how this can be used to cache lookups.
    pub fn insert_special<M>(
        &self,
        special_name: &str,
        data: Data,
        merge: M,
    ) -> Result<(Tag, Data)>
    where
        M: FnOnce(Data, Option<Data>) -> Result<Data>,
    {
        let blocking = true;
        pgp::plausible_tsk_or_tpk(&data)?;
        self.insert_impl(special_name, data, merge, blocking)
    }

    /// Inserts or updates the cert or key stored under a special
    /// name, non-blocking variant.
    ///
    /// Requires the special name, the cert or key in binary format and a
    /// callback function.  The callback is invoked with an `Option<Data>` of
    /// the existing data (if any), and is expected to merge the two copies
    /// together.  The returned `Data` is written to the
    /// store under the special name.  (Note: The function may decide to omit
    /// (parts of) the existing data, but this should be done with great care as
    /// not to lose any vital information.)
    ///
    /// Attempts to lock the store. Does not block, instead returns an
    /// [`Error::IoError`] with an [`std::io::ErrorKind::WouldBlock`]
    /// if the lock cannot be acquired.
    ///
    /// The insertion method returns the merged data and the tag.
    /// See [`Tag`] for how this can be used to cache lookups.
    pub fn try_insert_special<M>(
        &self,
        special_name: &str,
        data: Data,
        merge: M,
    ) -> Result<(Tag, Data)>
    where
        M: FnOnce(Data, Option<Data>) -> Result<Data>,
    {
        let blocking = false;
        pgp::plausible_tsk_or_tpk(&data)?;
        self.insert_impl(special_name, data, merge, blocking)
    }

    fn insert_impl<M>(
        &self,
        name: &str,
        data: Data,
        merge: M,
        blocking: bool,
    ) -> Result<(Tag, Data)>
    where
        M: FnOnce(Data, Option<Data>) -> Result<Data>,
    {
        let target_path = self.get_path(name)?;
        // Make sure the directory exists.
        fs::create_dir_all(target_path.parent().expect("at least one leg"))?;

        let mut lf = RwLock::new(self.idempotent_create_lockfile()?);
        // Lock exclusively
        let lock = if blocking {
            lf.write()?
        } else {
            lf.try_write()?
        };

        let old_cert = self.get(name)?.map(|(_, cert)| cert);
        let new_cert = merge(data, old_cert)?;

        {
            let mut tmp = NamedTempFile::new_in(&self.base)?;
            tmp.write_all(new_cert.as_ref())?;
            tmp.persist(&target_path).map_err(|e| e.error)?;
        }

        let tag = fs::File::open(&target_path)?.metadata()?.try_into()?;

        drop(lock);

        Ok((tag, new_cert))
    }

    /// Iterates over the certs in the store returning their fingerprints.
    pub fn fingerprints(&self) -> impl Iterator<Item = Result<String>> + '_ {
        WalkDir::new(&self.base)
            // take only subdirs of depth 2
            .max_depth(2)
            .min_depth(2)
            .into_iter()
            // Convert the paths to fingerprints. The store is a shared
            // directory, writable by anyone, so there may be files that don't
            // correspond to a fingerprint. Filter them out.
            .filter_map(move |e| match e {
                Ok(entry) => match self.get_fp_by_path(entry.path()) {
                    Ok(fp) => Some(Ok(fp)),
                    Err(_) => None,
                },
                Err(err) => Some(Err(err)),
            })
            .map(|e| e.map_err(Error::from))
    }

    /// Iterates over the certs in the store.
    ///
    /// Iterates over the certs in the store returning fingerprints,
    /// tags, and the data for each cert.
    pub fn iter(
        &self,
    ) -> impl Iterator<Item = Result<(String, Tag, Data)>> + '_ {
        // Helper function analogous to get, with the fingerprint included in
        // the output.
        let get_with_fp = move |fp: &str| -> Result<(String, Tag, Data)> {
            match self.get(fp)? {
                None => Err(Error::IoError(io::Error::new(
                    io::ErrorKind::Other,
                    // The file was found when fingerprints() walked over the
                    // directory, but wasn't found for reading now.
                    format!("The file for {} disappeared.", fp),
                ))),
                Some((tag, data)) => Ok((fp.to_owned(), tag, data)),
            }
        };

        self.fingerprints()
            .map(move |fp_result| fp_result.and_then(|fp| get_with_fp(&fp)))
    }

    fn idempotent_create_lockfile(&self) -> Result<std::fs::File> {
        let lock_path = self.base.join("writelock");
        // Open the lockfile for writing, and create it if it does not exist yet.
        std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(&lock_path)
            .map_err(Into::into)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use assert_fs::prelude::*;
    use predicates::prelude::*;

    use crate::TRUST_ROOT;

    fn test_base() -> assert_fs::TempDir {
        let base = assert_fs::TempDir::new().unwrap();
        match std::env::var_os("CERTD_TEST_PERSIST") {
            Some(_) => {
                eprintln!("Test base dir: {}", &base.path().to_string_lossy());
                base.into_persistent()
            }
            None => base,
        }
    }

    struct Testdata<'a> {
        data: &'a [u8],
        fingerprint: &'a str,
    }

    impl Testdata<'_> {
        fn path(&self) -> String {
            [&self.fingerprint[..2], &self.fingerprint[2..]].join("/")
        }

        fn add_to_certd(&self, base: &assert_fs::TempDir) {
            base.child(self.path()).write_binary(self.data).unwrap();
        }
    }

    static ALICE: Testdata = Testdata {
        fingerprint: "eb85bb5fa33a75e15e944e63f231550c4f47e38e",
        data: include_bytes!("../../testdata/alice.asc"),
    };

    static BOB: Testdata = Testdata {
        fingerprint: "d1a66e1a23b182c9980f788cfbfcc82a015e7330",
        data: include_bytes!("../../testdata/bob.asc"),
    };

    static TESTY: Testdata = Testdata {
        fingerprint: "39d100ab67d5bd8c04010205fb3751f1587daef1",
        data: include_bytes!("../../testdata/testy-new.pgp"),
    };

    fn setup_testdir(
        testdata: &[&Testdata],
    ) -> Result<(assert_fs::TempDir, CertD)> {
        let base = test_base();
        for t in testdata.iter() {
            t.add_to_certd(&base);
        }

        let trust_root_data = include_bytes!("../../testdata/sender.pgp");
        base.child("trust-root")
            .write_binary(trust_root_data)
            .unwrap();

        let certd = CertD::with_base_dir(&base)?;
        Ok((base, certd))
    }

    #[test]
    fn get_fp() -> std::result::Result<(), Box<dyn std::error::Error>> {
        let data = include_bytes!("../../testdata/testy-new.pgp");

        let base = test_base();
        base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1")
            .write_binary(data)
            .unwrap();

        let certd = CertD::with_base_dir(&base)?;

        let (tag, cert) = certd
            .get("39d100ab67d5bd8c04010205fb3751f1587daef1")?
            .unwrap();
        assert_eq!(cert.as_ref(), data);

        assert!(certd
            .get_if_changed(tag, "39d100ab67d5bd8c04010205fb3751f1587daef1")?
            .is_none());

        base.close().unwrap();
        Ok(())
    }

    #[test]
    fn get_special() -> std::result::Result<(), Box<dyn std::error::Error>> {
        let data = include_bytes!("../../testdata/sender.pgp");

        let base = test_base();
        base.child("trust-root").write_binary(data).unwrap();

        let certd = CertD::with_base_dir(&base)?;

        let (tag, cert) = certd.get(TRUST_ROOT)?.unwrap();
        assert_eq!(cert.as_ref(), data);

        assert!(certd.get_if_changed(tag, TRUST_ROOT)?.is_none());

        base.close().unwrap();
        Ok(())
    }

    #[test]
    fn get_not_found() -> Result<()> {
        let base = test_base();
        let certd = CertD::with_base_dir(&base)?;
        let result = certd.get("39d100ab67d5bd8c04010205fb3751f1587daef1");
        assert!(matches!(result, Ok(None)));
        Ok(())
    }

    #[test]
    fn insert_locked() -> Result<()> {
        let data = include_bytes!("../../testdata/testy-new.pgp");
        let base = test_base();

        let file = base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1");
        file.assert(predicate::path::missing());

        let certd = CertD::with_base_dir(&base)?;
        let f = |new: Data, old: Option<Data>| {
            assert!(old.is_none());
            Ok(new)
        };

        // Lock the lockfile before we try to insert
        let mut lf = RwLock::new(certd.idempotent_create_lockfile()?);
        // Lock exclusively
        let _lock = lf.write()?;

        let result = certd.try_insert(data.to_vec().into_boxed_slice(), &f);

        match result.unwrap_err() {
            Error::IoError(e) if e.kind() == io::ErrorKind::WouldBlock => {
                Ok(())
            }
            e => Err(e),
        }
    }

    #[test]
    fn insert_special_locked() -> Result<()> {
        let data = include_bytes!("../../testdata/sender.pgp");
        let base = test_base();

        let file = base.child("trust-root");
        file.assert(predicate::path::missing());

        let certd = CertD::with_base_dir(&base)?;
        let f = |new: Data, old: Option<Data>| {
            assert!(old.is_none());
            Ok(new)
        };

        // Lock the lockfile before we try to insert
        let mut lock = RwLock::new(certd.idempotent_create_lockfile()?);
        // Lock exclusively
        let _lock = lock.write()?;

        let result = certd.try_insert_special(
            TRUST_ROOT,
            data.to_vec().into_boxed_slice(),
            &f,
        );

        match result.unwrap_err() {
            Error::IoError(e) if e.kind() == io::ErrorKind::WouldBlock => {
                Ok(())
            }
            e => Err(e),
        }
    }

    #[test]
    fn insert_new() -> Result<()> {
        let data = include_bytes!("../../testdata/testy-new.pgp");
        let base = test_base();

        let file = base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1");
        file.assert(predicate::path::missing());

        let certd = CertD::with_base_dir(&base)?;

        let f = |new: Data, old: Option<Data>| {
            assert!(old.is_none());
            Ok(new)
        };

        let (_, inserted) =
            certd.insert(data.to_vec().into_boxed_slice(), &f)?;
        file.assert(data.as_ref());
        assert_eq!(inserted, data.to_vec().into_boxed_slice());

        Ok(())
    }

    #[test]
    fn insert_special_new() -> Result<()> {
        let data = include_bytes!("../../testdata/sender.pgp");
        let base = test_base();

        let file = base.child("trust-root");
        file.assert(predicate::path::missing());

        let certd = CertD::with_base_dir(&base)?;

        let f = |new: Data, old: Option<Data>| {
            assert!(old.is_none());
            Ok(new)
        };

        let (_, inserted) = certd.insert_special(
            "trust-root",
            data.to_vec().into_boxed_slice(),
            &f,
        )?;
        file.assert(data.as_ref());
        assert_eq!(inserted, data.to_vec().into_boxed_slice());

        Ok(())
    }

    #[test]
    fn insert_update() -> std::result::Result<(), Box<dyn std::error::Error>> {
        let data = include_bytes!("../../testdata/testy-new.pgp");
        let base = test_base();

        let file = base.child("39/d100ab67d5bd8c04010205fb3751f1587daef1");
        file.touch().unwrap();
        file.assert(predicate::str::is_empty());

        let certd = CertD::with_base_dir(&base)?;

        let f = |new: Data, old: Option<Data>| {
            assert!(old.is_some());
            Ok(new)
        };

        let (_, inserted) =
            certd.insert(data.to_vec().into_boxed_slice(), &f)?;
        file.assert(data.as_ref());
        assert_eq!(inserted, data.to_vec().into_boxed_slice());

        Ok(())
    }

    #[test]
    fn insert_special_update(
    ) -> std::result::Result<(), Box<dyn std::error::Error>> {
        let data = include_bytes!("../../testdata/sender.pgp");
        let base = test_base();

        let file = base.child("trust-root");
        file.touch().unwrap();
        file.assert(predicate::str::is_empty());

        let certd = CertD::with_base_dir(&base)?;

        let f = |new: Data, old: Option<Data>| {
            assert!(old.is_some());
            Ok(new)
        };

        let (_, inserted) = certd.insert_special(
            TRUST_ROOT,
            data.to_vec().into_boxed_slice(),
            &f,
        )?;
        file.assert(data.as_ref());
        assert_eq!(inserted, data.to_vec().into_boxed_slice());

        Ok(())
    }

    #[test]
    fn insert_get() -> std::result::Result<(), Box<dyn std::error::Error>> {
        let data = include_bytes!("../../testdata/testy-new.pgp");
        let base = test_base();

        let certd = CertD::with_base_dir(&base)?;

        let f = |new: Data, old: Option<Data>| {
            assert!(old.is_none());
            Ok(new)
        };

        certd.insert(data.to_vec().into_boxed_slice(), &f)?;
        let (_, cert) = certd
            .get("39d100ab67d5bd8c04010205fb3751f1587daef1")?
            .unwrap();
        assert_eq!(cert.as_ref(), data);

        Ok(())
    }

    #[test]
    fn get_path_by_fp() -> Result<()> {
        let base = test_base();
        let certd = CertD::with_base_dir(&base)?;

        let expected = base
            .path()
            .join("39")
            .join("d100ab67d5bd8c04010205fb3751f1587daef1");

        let fingerprint = "39d100ab67d5bd8c04010205fb3751f1587daef1";
        assert_eq!(certd.get_path_by_fp(fingerprint)?, expected);

        let fingerprint = "39D100AB67D5BD8C04010205FB3751F1587DAEF1";
        assert_eq!(certd.get_path_by_fp(fingerprint)?, expected);

        let fingerprint = "39D100ab67D5bD8C04010205FB3751f1587DAeF1";
        assert_eq!(certd.get_path_by_fp(fingerprint)?, expected);

        Ok(())
    }

    #[test]
    fn get_path_by_fp_negative() -> Result<()> {
        let base = test_base();
        let certd = CertD::with_base_dir(&base)?;

        // empty
        let fingerprint = "";
        let result = certd.get_path_by_fp(fingerprint);
        assert!(matches!(result.unwrap_err(), Error::BadName));

        // too short
        let fingerprint = "39d100ab67d5bd8c04010205fb3751f1587daef";
        let result = certd.get_path_by_fp(fingerprint);
        assert!(matches!(result.unwrap_err(), Error::BadName));

        // not ascii hex
        let fingerprint = "peter";
        let result = certd.get_path_by_fp(fingerprint);
        assert!(matches!(result.unwrap_err(), Error::BadName));
        Ok(())
    }

    #[test]
    fn get_path_by_special() -> Result<()> {
        let base = test_base();
        let certd = CertD::with_base_dir(&base)?;

        let expected = base.path().join(TRUST_ROOT);

        let name = "trust-root";
        assert_eq!(certd.get_path_by_special(name)?, expected);

        let name = "TRUST-ROOT";
        assert_eq!(certd.get_path_by_special(name)?, expected);

        let name = "TrUsT-RooT";
        assert_eq!(certd.get_path_by_special(name)?, expected);

        Ok(())
    }

    #[test]
    fn get_path_by_special_negative() -> Result<()> {
        let base = test_base();
        let certd = CertD::with_base_dir(&base)?;

        // empty
        let name = "";
        let result = certd.get_path_by_special(name);
        assert!(matches!(result.unwrap_err(), Error::BadName));

        // unknown
        let name = "mySpecialName";
        let result = certd.get_path_by_special(name);
        assert!(matches!(result.unwrap_err(), Error::BadName));
        Ok(())
    }

    #[test]
    fn fingerprints() -> Result<()> {
        use std::collections::HashSet;

        let (_base, certd) = setup_testdir(&[&ALICE, &BOB, &TESTY])?;

        let iter_fp = certd.fingerprints();
        let fps = iter_fp.collect::<Result<HashSet<_>>>()?;
        let expected: HashSet<_> =
            [ALICE.fingerprint, BOB.fingerprint, TESTY.fingerprint]
                .iter()
                .map(|&s| s.to_owned())
                .collect();
        assert_eq!(expected, fps);

        Ok(())
    }

    #[test]
    fn fingerprints_empty() -> Result<()> {
        use std::collections::HashSet;

        let base = test_base();
        let certd = CertD::with_base_dir(&base)?;

        let iter_fp = certd.fingerprints();
        let fps = iter_fp.collect::<Result<HashSet<_>>>()?;
        assert!(fps.is_empty());

        Ok(())
    }

    #[test]
    fn fingerprints_junk() -> Result<()> {
        use std::collections::HashSet;

        let (base, certd) = setup_testdir(&[&ALICE, &BOB, &TESTY])?;
        base.child("some_file").write_str("some_text").unwrap();
        base.child("aa/some_file").write_str("some_text").unwrap();
        base.child("aa/aa/some_file")
            .write_str("some_text")
            .unwrap();

        let iter_fp = certd.fingerprints();
        let fps = iter_fp.collect::<Result<HashSet<_>>>()?;
        let expected: HashSet<_> =
            [ALICE.fingerprint, BOB.fingerprint, TESTY.fingerprint]
                .iter()
                .map(|&s| s.to_owned())
                .collect();
        assert_eq!(expected, fps);

        Ok(())
    }

    #[test]
    fn iter() -> Result<()> {
        use std::collections::HashSet;

        let (_base, certd) = setup_testdir(&[&ALICE, &BOB, &TESTY])?;

        let mut expected: HashSet<_> = [&ALICE, &BOB, &TESTY]
            .iter()
            .map(|&s| {
                (
                    s.fingerprint.to_owned(),
                    certd.get(s.fingerprint).unwrap().unwrap().0,
                    s.data.to_vec().into_boxed_slice(),
                )
            })
            .collect();

        for item in certd.iter() {
            let item = item?;
            assert!(expected.contains(&item));
            expected.remove(&item);
        }
        assert!(expected.is_empty());

        Ok(())
    }

    #[test]
    fn base_path() -> Result<()> {
        let base = assert_fs::TempDir::new().unwrap();
        let certd = CertD::with_base_dir(&base)?;

        assert_eq!(certd.get_base_dir(), base.path());
        Ok(())
    }
}
