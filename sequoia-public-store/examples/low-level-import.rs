//! Demonstrates how to import a certring.

use openpgp::{cert::prelude::*, parse::Parse, serialize::SerializeInto, *};
use openpgp_cert_d::Data;
use sequoia_openpgp as openpgp;

fn main() -> Result<()> {
    let certd = openpgp_cert_d::CertD::new()?;

    let args = std::env::args().collect::<Vec<_>>();
    if args.len() != 1 {
        panic!("Usage: {0} <CERTRING  -or-  gpg --export | {0}", args[0]);
    }

    for cert in CertParser::from_reader(std::io::stdin())? {
        let cert = cert?;

        let merge_fn = |new: Data, old: Option<Data>| {
            let merged = match old {
                Some(old) => {
                    let old = Cert::from_bytes(&old)?;
                    let new = Cert::from_bytes(&new)?;
                    old.merge_public(new)?.to_vec()?.into_boxed_slice()
                }
                None => new,
            };
            Ok(merged)
        };

        certd.insert(cert.to_vec()?.into_boxed_slice(), merge_fn)?;
    }

    Ok(())
}
