//! Demonstrates how to insert a certificate.

use openpgp::{parse::Parse, serialize::SerializeInto, *};
use openpgp_cert_d::Data;
use sequoia_openpgp as openpgp;

fn main() -> Result<()> {
    let certd = openpgp_cert_d::CertD::new()?;

    let args = std::env::args().collect::<Vec<_>>();
    if args.len() != 2 {
        panic!("Usage: {} <CERTFILE>", args[0]);
    }

    let cert = Cert::from_file(&args[1])?;

    let merge_fn = |new: Data, old: Option<Data>| {
        let merged = match old {
            Some(old) => {
                let old = Cert::from_bytes(&old)?;
                let new = Cert::from_bytes(&new)?;
                old.merge_public(new)?.to_vec()?.into_boxed_slice()
            }
            None => new,
        };
        Ok(merged)
    };

    certd.insert(cert.to_vec()?.into_boxed_slice(), merge_fn)?;
    Ok(())
}
