# Shared OpenPGP Certificate Directory

This document defines a generic OpenPGP certificate store that can be
shared between implementations.  It also defines a way to root trust,
and a way to associate pet names with certificates.  Sharing
certificates and trust decisions increases security by enabling more
applications to take advantage of OpenPGP.  It also improves privacy
by reducing the required certificate discoveries that go out to the
network.

It is currently published with the IETF at https://datatracker.ietf.org/doc/draft-nwjw-openpgp-cert-d/

You might also want to see the latest [editor's copy][https://sequoia-pgp.gitlab.io/pgp-cert-d/].

Furthermore, you can read the latest [API documentation][https://sequoia-pgp.gitlab.io/pgp-cert-d/api].
